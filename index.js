// Load the expressjs module into our application and saved it in a variable called express
const express = require("express")
const port = 4000

// app is our server
// create an application that uses express and stores it as app
const app = express();

// Middleware (request handlers)
// express.json() is a method which allows us to handle the streaming of data and automatically parse the incoming JSON from our req.body


app.use(express.json());



//mock data

let users = [
    {
        username: "mbbanaag",
        email: "banaagmb@mail.com",
        password: "password123"
    },
    {
        username: "banaagmiko",
        email: "banaagmiko@mail.com",
        password: "password1234"
    }
]

let items = [
    {
        name: "Mjolnir",
        price: 5000,
        isActive: true
    },
    {
        name: "Vibranium",
        price: 7000,
        isActive: true
    }
]

app.get('/', (req, res) => {
    res.send('Hello from my first ExpressJS API');
})

// app.get
// app.post
// app.put
// app.delete
// app.all
// app.use
// app.listen


app.get('/users', (req, res)=> {
    res.send(users);
})

app.post('/users', (req, res)=> {
    
    let newUser = {

        username: req.body.username,
        email: req.body.email,
        password: req.body.password

    }

    users.push(newUser);
    console.log(users);

    res.send(users)
})


app.put('/users/:index', (req,res) => {

    console.log(req.body);

    console.log(req.params);

    let index = parseInt(req.params.index);

    users[index].password = req.body.password;
    res.send(users[index]);
})

app.delete('/users' , )



// ACTIVITY ----------------------------------------------------------



app.get('/items', (req, res)=> {
    res.send(items);
})

app.post('/items', (req, res)=> {
    
    let newItem = {

        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive

    }

    items.push(newItem);
    console.log(items);

    res.send(items)
})


app.put('/items/:index', (req,res) => {

    console.log(req.body);

    console.log(req.params);

    let index = parseInt(req.params.index);

    items[index].price = req.body.price;
    res.send(items[index]);
})






app.listen(port, () => {
    console.log(`Server is listening at port ${port}`)
})